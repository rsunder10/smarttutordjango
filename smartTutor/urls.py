from django.conf.urls import url,include
from django.contrib import admin
from myProfile import views
from django.conf import settings
from django.conf.urls.static import static
from quiz import views as quiz
from myrec import views as myrec

urlpatterns = [
    url(r'^$', views.index,name='account_index' ),
    url(r'^learn/', include('simpleblog.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/profile',views.profile,name='account_profile' ),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^quiz/',quiz.index,name='quiz_categories'),
    url(r'^category/(?P<category_id>\d+)', quiz.view_category,name='quiz_category'),
    url(r'^take/(?P<quiz_id>\d+)/$', quiz.quiz_take,name='quiz_take'),
    url(r'^recommendProduct/',myrec.recommendProduct,name='recommend'),
    url(r'^votes/',myrec.vote,name='vote')
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
