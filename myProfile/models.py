from __future__ import unicode_literals

from django.db import models
from django.conf import settings

# Create your models here.
class Experience(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)
    sentiment = models.BooleanField()
