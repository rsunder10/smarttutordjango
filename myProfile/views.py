from django.shortcuts import render
from .models import Experience


#!/usr/bin/python

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser



#!/usr/bin/python

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
from recommends.storages.djangoorm.models import Recommendation


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "AIzaSyCGJ1bRPnw1-uu2NpRuld7q_82nhbZUMiQ"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def youtube_search(term,max):
  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  # Call the search.list method to retrieve results matching the specified
  # query term.
  search_response = youtube.search().list(
    q=term,
    part="id,snippet",
    maxResults=max
  ).execute()

  videos = []

  channels = []

  playlists = []


  # Add each result to the appropriate list, and then display the lists of
  # matching videos, channels, and playlists.
  for search_result in search_response.get("items", []):
    if search_result["id"]["kind"] == "youtube#video":
      subvideos=[]
      subvideos.append(search_result["snippet"]["title"])
      subvideos.append(search_result["id"]["videoId"])
      videos.append(subvideos)
    elif search_result["id"]["kind"] == "youtube#channel":
      subchannels=[]
      subchannels.append(search_result["snippet"]["title"])
      subchannels.append(search_result["id"]["channelId"])
      channels.append(subchannels)
    elif search_result["id"]["kind"] == "youtube#playlist":
      subplaylists =[]
      subplaylists.append(search_result["snippet"]["title"])
      subplaylists.append(search_result["id"]["playlistId"])
      playlists.append(subplaylists)

  return videos,channels,playlists
# Create your views here.
def index(request):
  return render(request, "account/index.html")
def profile(request):
  experience = Experience.objects.all().filter(user = request.user)
  total = 0
  print(experience)
  enjoy = 0
  upset = 0
  for i in experience:
    print(i.sentiment)
    if(i.sentiment == True):
      enjoy+=1
    else:
      upset += 1
  total =0.0
  total =total+upset+enjoy
  if(enjoy !=0):
    experience = (enjoy/total)*100
  else:
    experience=0
  if(upset!=0):
    danger = (upset/total)*100
  else:
    danger=0

  recommends = Recommendation.objects.all().filter(user=request.user.id).order_by('?')[0:2]
  listOfRec=[]
  for eachRec in recommends:
    videos,channels,playlists = youtube_search(eachRec.object.name+" tutorial",5)
    listOfRec.append(videos)
  return render(request, "account/profile.html",{'enjoy':experience,'upset':danger,'listOfRec':listOfRec})
